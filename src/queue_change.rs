use glib::GEnum;
use num_enum::TryFromPrimitive;

#[derive(Debug, Copy, Clone, GEnum, PartialEq, TryFromPrimitive)]
#[repr(u32)]
#[genum(type_name = "TrQueueChange")]
pub enum TrQueueChange {
    MoveTop = 0,
    MoveUp = 1,
    MoveDown = 2,
    MoveBottom = 3,
}

impl Default for TrQueueChange {
    fn default() -> Self {
        Self::MoveTop
    }
}
