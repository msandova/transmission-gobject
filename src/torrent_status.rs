use glib::GEnum;
use num_enum::TryFromPrimitive;

#[derive(Debug, Copy, Clone, GEnum, PartialEq, TryFromPrimitive)]
#[repr(u32)]
#[genum(type_name = "TrTorrentStatus")]
pub enum TrTorrentStatus {
    Stopped = 0,
    CheckWait = 1,
    Check = 2,
    DownloadWait = 3,
    Download = 4,
    SeedWait = 5,
    Seed = 6,
}

impl Default for TrTorrentStatus {
    fn default() -> Self {
        Self::Stopped
    }
}
