use async_channel::{Receiver, Sender};
use glib::subclass::prelude::ObjectSubclass;
use glib::subclass::prelude::*;
use glib::subclass::signal::Signal;
use glib::{clone, ObjectExt, ParamFlags, ParamSpec, StaticType, ToValue};
use once_cell::sync::Lazy;
use transmission_client::{Client, ClientError};
use url::Url;

use std::cell::RefCell;

use crate::torrent::TrTorrent;
use crate::torrent_model::TrTorrentModel;

mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct TrClient {
        pub address: RefCell<String>,
        pub polling_rate: RefCell<u64>,
        pub torrents: TrTorrentModel,

        pub client: RefCell<Option<Client>>,

        pub do_connect: RefCell<bool>,
        pub do_disconnect: RefCell<bool>,

        pub sender: Sender<bool>,
        pub receiver: Receiver<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TrClient {
        const NAME: &'static str = "TrClient";
        type ParentType = glib::Object;
        type Type = super::TrClient;

        fn new() -> Self {
            let (sender, receiver) = async_channel::bounded(1);

            Self {
                address: RefCell::default(),
                polling_rate: RefCell::default(),
                torrents: TrTorrentModel::default(),
                client: RefCell::default(),
                do_connect: RefCell::default(),
                do_disconnect: RefCell::default(),
                sender,
                receiver,
            }
        }
    }

    impl ObjectImpl for TrClient {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
                vec![
                    // Triggered when a connection has been established
                    Signal::builder("connected", &[], glib::Type::UNIT.into()).build(),
                    // Triggered when a connection has been disconnected
                    // Returns a `String` with an error message, is empty when it got disconnected gracefully
                    Signal::builder(
                        "disconnected",
                        &[glib::Type::STRING.into()],
                        glib::Type::UNIT.into(),
                    )
                    .build(),
                ]
            });
            SIGNALS.as_ref()
        }

        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpec::new_string(
                        "address",
                        "Address",
                        "Address",
                        None,
                        ParamFlags::READABLE,
                    ),
                    ParamSpec::new_uint64(
                        "polling-rate",
                        "Polling rate",
                        "Polling rate",
                        0,
                        std::u64::MAX,
                        0,
                        ParamFlags::READABLE,
                    ),
                    ParamSpec::new_object(
                        "torrents",
                        "Torrents",
                        "Torrents",
                        TrTorrentModel::static_type(),
                        ParamFlags::READABLE,
                    ),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                "address" => self.address.borrow().to_value(),
                "polling-rate" => self.polling_rate.borrow().to_value(),
                "torrents" => self.torrents.to_value(),
                _ => unimplemented!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct TrClient(ObjectSubclass<imp::TrClient>);
}

impl TrClient {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub async fn connect(&self, address: String, polling_rate: u64) -> Result<(), ClientError> {
        let imp = imp::TrClient::from_instance(&self);

        if *imp.do_connect.borrow() || *imp.do_disconnect.borrow() {
            warn!("Client is currently busy, unable to connect to new address.");
            return Ok(());
        }

        // With the do_connect/disconnect variables we avoid race conditions
        // eg. doing another connect attempt, while the client is already busy
        *imp.do_connect.borrow_mut() = true;

        // Do the actual connecting work
        let result = self.connect_internal(address, polling_rate).await;

        // Work is done -> unblock it again.
        *imp.do_connect.borrow_mut() = false;

        result
    }

    async fn connect_internal(
        &self,
        address: String,
        polling_rate: u64,
    ) -> Result<(), ClientError> {
        let imp = imp::TrClient::from_instance(&self);

        if self.is_connected() {
            self.disconnect(false, None).await;
        }

        debug!("Connect to {} ...", address);

        // Create new client for the rpc communication
        let url = Url::parse(&address).unwrap();
        let client = Client::new(url);

        // Set properties
        *imp.address.borrow_mut() = address.clone();
        *imp.polling_rate.borrow_mut() = polling_rate;
        self.notify("address");
        self.notify("polling-rate");

        // Check server version
        let session = client.session().await?;
        debug!(
            "Connected to transmission session version {}",
            session.version
        );

        // Make first poll
        *imp.client.borrow_mut() = Some(client.clone());
        self.poll_data().await?;
        self.emit_by_name("connected", &[]).unwrap();

        // Start polling
        let duration = std::time::Duration::from_millis(polling_rate);
        glib::timeout_add_local(
            duration,
            clone!(@weak self as this => @default-return glib::Continue(false), move ||{
                let imp = imp::TrClient::from_instance(&this);
                let disconnect = imp.do_disconnect.borrow().clone();

                let fut = clone!(@weak this, @strong disconnect => async move {
                    let imp = imp::TrClient::from_instance(&this);
                    if disconnect {
                        debug!("Stop polling loop...");

                        // Send message back to the disconnect method to indicate that the
                        // polling loop has been stopped
                        let sender = imp.sender.clone();
                        sender.send(true).await.unwrap();

                        *imp.do_disconnect.borrow_mut() = false;
                    }else{
                        if let Err(err) = this.poll_data().await{
                            warn!("Couldn't poll transmission data: {}", err.to_string());
                            this.clone().disconnect(false, Some(err.to_string())).await;
                        };
                    }
                });
                spawn!(fut);

                glib::Continue(!disconnect)
            }),
        );

        Ok(())
    }

    pub async fn disconnect(&self, close_session: bool, message: Option<String>) {
        let imp = imp::TrClient::from_instance(&self);
        if !self.is_connected() {
            warn!("Unable to disconnect, is not connected.");
            return;
        }

        if close_session {
            let client = imp.client.borrow().as_ref().unwrap().clone();
            client.session_close().await.unwrap();
        }

        // Wait till polling loop has stopped
        *imp.do_disconnect.borrow_mut() = true;

        // Wait from message from polling loop to ensure we're not polling anymore.
        let r = imp.receiver.recv().await.unwrap();
        debug!("Stopped polling: {:?}", r);

        let _client = imp.client.borrow_mut().take().unwrap();
        self.emit_by_name("disconnected", &[&message.unwrap_or("".into())])
            .unwrap();

        // Not connected anymore -> clear torrents model
        imp.torrents.clear();

        debug!("Disconnected from transmission session");
    }

    pub async fn add_new_torrent(&self, filename: String) -> Result<(), ClientError> {
        if let Some(client) = self.rpc_client() {
            client.torrent_add(&filename).await?;
        } else {
            warn!("Unable to add new torrent, no rpc connection");
        }

        Ok(())
    }

    async fn poll_data(&self) -> Result<(), ClientError> {
        let imp = imp::TrClient::from_instance(self);

        if let Some(client) = self.rpc_client() {
            let torrents = client.torrents(None).await?;

            // We have to find out which torrent isn't included anymore,
            // so we can remove it from the model too.
            let mut hashes_delta = imp.torrents.get_hashes();

            for rpc_torrent in torrents {
                let hash = rpc_torrent.hash_string.clone();

                if let Some(torrent) = imp.torrents.torrent_by_hash(hash.clone()) {
                    // Check if torrent status or queue position changed
                    if torrent.status() as u32 != rpc_torrent.status as u32
                        || torrent.queue_position() != rpc_torrent.queue_position
                    {
                        // Update torrent properties
                        torrent.refresh_values(rpc_torrent);

                        // ... yes -> emit torrent model `status-changed` signal
                        imp.torrents
                            .emit_by_name("status-changed", &[])
                            .expect("Could not emit status-changed signal.");
                    } else {
                        // Update torrent properties, without emiting the status-changed signal
                        torrent.refresh_values(rpc_torrent);
                    }

                    let index = hashes_delta.iter().position(|x| *x == hash).unwrap();
                    hashes_delta.remove(index);
                } else {
                    debug!("Append new torrent: {}", rpc_torrent.name);
                    imp.torrents
                        .add_torrent(&TrTorrent::from_rpc_torrent(rpc_torrent, self.clone()));
                }
            }

            // Remove the deltas from the model
            for hash in hashes_delta {
                let torrent = imp.torrents.torrent_by_hash(hash).unwrap();
                imp.torrents.remove_torrent(&torrent);
            }
        } else {
            warn!("Unable to poll transmission data, no rpc connection.");
        }

        Ok(())
    }

    pub(crate) fn rpc_client(&self) -> Option<Client> {
        let imp = imp::TrClient::from_instance(self);
        imp.client.borrow().clone()
    }

    pub fn address(&self) -> String {
        self.property("address").unwrap().get().unwrap()
    }

    pub fn polling_rate(&self) -> u64 {
        self.property("polling-rate").unwrap().get().unwrap()
    }

    pub fn is_connected(&self) -> bool {
        self.rpc_client().is_some()
    }

    pub fn torrents(&self) -> TrTorrentModel {
        self.property("torrents").unwrap().get().unwrap()
    }
}
