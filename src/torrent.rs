use glib::subclass::prelude::ObjectSubclass;
use glib::subclass::prelude::*;
use glib::{ObjectExt, ParamSpec, StaticType, ToValue};
use once_cell::sync::Lazy;
use once_cell::sync::OnceCell;
use transmission_client::{ClientError, Torrent};

use std::cell::RefCell;
use std::convert::TryFrom;

use crate::client::TrClient;
use crate::queue_change::TrQueueChange;
use crate::torrent_status::TrTorrentStatus;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct TrTorrent {
        pub client: OnceCell<TrClient>,

        /* Static values */
        pub name: OnceCell<String>,
        pub hash: OnceCell<String>,

        /* Dynamic values */
        pub download_dir: RefCell<String>,
        pub size: RefCell<i64>,
        pub status: RefCell<TrTorrentStatus>,
        pub eta: RefCell<i64>,
        pub progress: RefCell<f32>,
        pub queue_position: RefCell<i64>,

        pub seeders_active: RefCell<i64>,
        pub seeders: RefCell<i64>,
        pub leechers: RefCell<i64>,

        pub downloaded: RefCell<i64>,
        pub uploaded: RefCell<i64>,
        pub download_speed: RefCell<i64>,
        pub upload_speed: RefCell<i64>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TrTorrent {
        const NAME: &'static str = "TrTorrent";
        type ParentType = glib::Object;
        type Type = super::TrTorrent;
    }

    impl ObjectImpl for TrTorrent {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpec::new_string("name", "Name", "Name", None, glib::ParamFlags::READABLE),
                    ParamSpec::new_string("hash", "Hash", "Hash", None, glib::ParamFlags::READABLE),
                    ParamSpec::new_string(
                        "download-dir",
                        "Download directory",
                        "Download directory",
                        None,
                        glib::ParamFlags::READABLE,
                    ),
                    ParamSpec::new_int64(
                        "size",
                        "Size",
                        "Size",
                        0,
                        i64::MAX,
                        0,
                        glib::ParamFlags::READABLE,
                    ),
                    ParamSpec::new_enum(
                        "status",
                        "Status",
                        "Status",
                        TrTorrentStatus::static_type(),
                        TrTorrentStatus::default() as i32,
                        glib::ParamFlags::READABLE,
                    ),
                    ParamSpec::new_int64(
                        "eta",
                        "ETA",
                        "ETA",
                        0,
                        i64::MAX,
                        0,
                        glib::ParamFlags::READABLE,
                    ),
                    ParamSpec::new_float(
                        "progress",
                        "Progress",
                        "Progress",
                        0.0,
                        f32::MAX,
                        0.0,
                        glib::ParamFlags::READABLE,
                    ),
                    ParamSpec::new_int64(
                        "queue-position",
                        "Queue position",
                        "Queue position",
                        0,
                        i64::MAX,
                        0,
                        glib::ParamFlags::READABLE,
                    ),
                    ParamSpec::new_int64(
                        "seeders-active",
                        "Seeders active",
                        "Seeders active",
                        0,
                        i64::MAX,
                        0,
                        glib::ParamFlags::READABLE,
                    ),
                    ParamSpec::new_int64(
                        "seeders",
                        "Seeders",
                        "Seeders",
                        0,
                        i64::MAX,
                        0,
                        glib::ParamFlags::READABLE,
                    ),
                    ParamSpec::new_int64(
                        "leechers",
                        "Leechers",
                        "Leechers",
                        0,
                        i64::MAX,
                        0,
                        glib::ParamFlags::READABLE,
                    ),
                    ParamSpec::new_int64(
                        "downloaded",
                        "Downloaded",
                        "Downloaded",
                        0,
                        i64::MAX,
                        0,
                        glib::ParamFlags::READABLE,
                    ),
                    ParamSpec::new_int64(
                        "uploaded",
                        "Uploaded",
                        "Uploaded",
                        0,
                        i64::MAX,
                        0,
                        glib::ParamFlags::READABLE,
                    ),
                    ParamSpec::new_int64(
                        "download-speed",
                        "Download speed",
                        "Download speed",
                        0,
                        i64::MAX,
                        0,
                        glib::ParamFlags::READABLE,
                    ),
                    ParamSpec::new_int64(
                        "upload-speed",
                        "Upload speed",
                        "Upload speed",
                        0,
                        i64::MAX,
                        0,
                        glib::ParamFlags::READABLE,
                    ),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                "name" => self.name.get().unwrap().to_value(),
                "hash" => self.hash.get().unwrap().to_value(),
                "download-dir" => self.download_dir.borrow().to_value(),
                "size" => self.size.borrow().to_value(),
                "status" => self.status.borrow().to_value(),
                "eta" => self.eta.borrow().to_value(),
                "progress" => self.progress.borrow().to_value(),
                "queue-position" => self.queue_position.borrow().to_value(),
                "seeders-active" => self.seeders_active.borrow().to_value(),
                "seeders" => self.seeders.borrow().to_value(),
                "leechers" => self.leechers.borrow().to_value(),
                "downloaded" => self.downloaded.borrow().to_value(),
                "uploaded" => self.uploaded.borrow().to_value(),
                "download-speed" => self.download_speed.borrow().to_value(),
                "upload-speed" => self.upload_speed.borrow().to_value(),
                _ => unimplemented!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct TrTorrent(ObjectSubclass<imp::TrTorrent>);
}

impl TrTorrent {
    pub fn from_rpc_torrent(rpc_torrent: Torrent, client: TrClient) -> Self {
        let torrent: Self = glib::Object::new(&[]).unwrap();
        let imp = imp::TrTorrent::from_instance(&torrent);

        imp.client.set(client).unwrap();

        // Set static values
        imp.hash.set(rpc_torrent.hash_string.clone()).unwrap();
        imp.name.set(rpc_torrent.name.clone()).unwrap();

        // Set dynamic values
        torrent.refresh_values(rpc_torrent);

        torrent
    }

    pub async fn start(&self, bypass_queue: bool) -> Result<(), ClientError> {
        let imp = imp::TrTorrent::from_instance(self);

        if let Some(client) = imp.client.get().unwrap().rpc_client() {
            client
                .torrent_start(Some(vec![self.hash()]), bypass_queue)
                .await?;
        } else {
            warn!("Unable to start torrent, no rpc connection.");
        }

        Ok(())
    }

    pub async fn stop(&self) -> Result<(), ClientError> {
        let imp = imp::TrTorrent::from_instance(self);

        if let Some(client) = imp.client.get().unwrap().rpc_client() {
            client.torrent_stop(Some(vec![self.hash()])).await?;
        } else {
            warn!("Unable to stop torrent, no rpc connection.");
        }

        Ok(())
    }

    pub async fn remove(&self, delete_local_data: bool) -> Result<(), ClientError> {
        let imp = imp::TrTorrent::from_instance(self);

        if let Some(client) = imp.client.get().unwrap().rpc_client() {
            client
                .torrent_remove(Some(vec![self.hash()]), delete_local_data)
                .await?;
        } else {
            warn!("Unable to stop torrent, no rpc connection.");
        }

        Ok(())
    }

    pub async fn reannounce(&self) -> Result<(), ClientError> {
        let imp = imp::TrTorrent::from_instance(self);

        if let Some(client) = imp.client.get().unwrap().rpc_client() {
            client.torrent_reannounce(Some(vec![self.hash()])).await?;
        } else {
            warn!("Unable to stop torrent, no rpc connection.");
        }

        Ok(())
    }

    pub async fn queue_change(&self, change: TrQueueChange) -> Result<(), ClientError> {
        let imp = imp::TrTorrent::from_instance(self);

        if let Some(client) = imp.client.get().unwrap().rpc_client() {
            match change {
                TrQueueChange::MoveTop => client.queue_move_top(Some(vec![self.hash()])).await?,
                TrQueueChange::MoveUp => client.queue_move_up(Some(vec![self.hash()])).await?,
                TrQueueChange::MoveDown => client.queue_move_down(Some(vec![self.hash()])).await?,
                TrQueueChange::MoveBottom => {
                    client.queue_move_bottom(Some(vec![self.hash()])).await?
                }
            }
        } else {
            warn!("Unable to modify torrent queue, no rpc connection.");
        }

        Ok(())
    }

    pub fn refresh_values(&self, rpc_torrent: Torrent) {
        let imp = imp::TrTorrent::from_instance(self);

        // download_dir
        if *imp.download_dir.borrow() != rpc_torrent.download_dir {
            *imp.download_dir.borrow_mut() = rpc_torrent.download_dir;
            self.notify("download-dir");
        }

        // size
        if *imp.size.borrow() != rpc_torrent.size_when_done {
            *imp.size.borrow_mut() = rpc_torrent.size_when_done;
            self.notify("size");
        }

        // status
        if *imp.status.borrow() as u32 != rpc_torrent.status as u32 {
            let status = TrTorrentStatus::try_from(rpc_torrent.status as u32).unwrap();
            *imp.status.borrow_mut() = status;
            self.notify("status");
        }

        // eta
        if *imp.eta.borrow() != rpc_torrent.eta {
            *imp.eta.borrow_mut() = rpc_torrent.eta;
            self.notify("eta");
        }

        // progress
        if *imp.progress.borrow() != rpc_torrent.percent_done {
            *imp.progress.borrow_mut() = rpc_torrent.percent_done;
            self.notify("progress");
        }

        // queue_position
        if *imp.queue_position.borrow() != rpc_torrent.queue_position {
            *imp.queue_position.borrow_mut() = rpc_torrent.queue_position;
            self.notify("queue_position");
        }

        // seeders_active
        if *imp.seeders_active.borrow() != rpc_torrent.peers_sending_to_us {
            *imp.seeders_active.borrow_mut() = rpc_torrent.peers_sending_to_us;
            self.notify("seeders_active");
        }

        // seeders
        if *imp.seeders.borrow() != rpc_torrent.peers_connected {
            *imp.seeders.borrow_mut() = rpc_torrent.peers_connected;
            self.notify("seeders");
        }

        // leechers
        if *imp.leechers.borrow() != rpc_torrent.peers_getting_from_us {
            *imp.leechers.borrow_mut() = rpc_torrent.peers_getting_from_us;
            self.notify("leechers");
        }

        // downloaded
        if *imp.downloaded.borrow() != rpc_torrent.have_valid {
            *imp.downloaded.borrow_mut() = rpc_torrent.have_valid;
            self.notify("downloaded");
        }

        // uploaded
        if *imp.uploaded.borrow() != rpc_torrent.uploaded_ever {
            *imp.uploaded.borrow_mut() = rpc_torrent.uploaded_ever;
            self.notify("uploaded");
        }

        // downloaded
        if *imp.download_speed.borrow() != rpc_torrent.rate_download {
            *imp.download_speed.borrow_mut() = rpc_torrent.rate_download;
            self.notify("download-speed");
        }

        // uploaded
        if *imp.upload_speed.borrow() != rpc_torrent.rate_upload {
            *imp.upload_speed.borrow_mut() = rpc_torrent.rate_upload;
            self.notify("upload-speed");
        }
    }

    pub fn name(&self) -> String {
        self.property("name").unwrap().get().unwrap()
    }

    pub fn hash(&self) -> String {
        self.property("hash").unwrap().get().unwrap()
    }

    pub fn download_dir(&self) -> String {
        self.property("download_dir").unwrap().get().unwrap()
    }

    pub fn size(&self) -> i64 {
        self.property("size").unwrap().get().unwrap()
    }

    pub fn status(&self) -> TrTorrentStatus {
        self.property("status").unwrap().get().unwrap()
    }

    pub fn eta(&self) -> i64 {
        self.property("eta").unwrap().get().unwrap()
    }

    pub fn progress(&self) -> f32 {
        self.property("progress").unwrap().get().unwrap()
    }

    pub fn queue_position(&self) -> i64 {
        self.property("queue_position").unwrap().get().unwrap()
    }

    pub fn seeders_active(&self) -> i64 {
        self.property("seeders_active").unwrap().get().unwrap()
    }

    pub fn seeders(&self) -> i64 {
        self.property("seeders").unwrap().get().unwrap()
    }

    pub fn leechers(&self) -> i64 {
        self.property("leechers").unwrap().get().unwrap()
    }

    pub fn downloaded(&self) -> i64 {
        self.property("downloaded").unwrap().get().unwrap()
    }

    pub fn uploaded(&self) -> i64 {
        self.property("uploaded").unwrap().get().unwrap()
    }

    pub fn download_speed(&self) -> i64 {
        self.property("download-speed").unwrap().get().unwrap()
    }

    pub fn upload_speed(&self) -> i64 {
        self.property("upload-speed").unwrap().get().unwrap()
    }
}
